Cannot make it on CI, so run it on local with podman and make as requirements

```
# watch out for this line, contexts where wrong but podman still succeeded using old built layers!!!
sudo podman build -f container/Dockerfile ./
sudo podman image tag <image_id_from_previous_step> gitlab-registry.cern.ch/djuarezg/faf:fullbuild
sudo podman image push gitlab-registry.cern.ch/djuarezg/faf:fullbuild
```

* Increase deployment resource limits

* Prepare it internally


```
faf releaseadd -o centos --opsys-release 7 --status ACTIVE
faf repoadd updates_debug dnf http://linuxsoft.cern.ch/cern/centos/7/updates/Debug/x86_64/ --nogpgcheck
faf repoassign updates_debug "CentOS 7" x86_64
faf repoadd updates dnf http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/ --nogpgcheck
faf repoassign updates "CentOS 7" x86_64
faf repoadd updates_source dnf http://linuxsoft.cern.ch/cern/centos/7/updates/Source/ --nogpgcheck
faf repoassign updates_source "CentOS 7" x86_64
faf repoadd updates_sources dnf http://linuxsoft.cern.ch/cern/centos/7/updates/Sources/ --nogpgcheck
faf repoassign updates_sources "CentOS 7" x86_64
faf repoadd os dnf http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/ --nogpgcheck
faf repoassign os "CentOS 7" x86_64
faf repoadd os_source dnf http://linuxsoft.cern.ch/cern/centos/7/os/Source/ --nogpgcheck
faf repoassign os_source "CentOS 7" x86_64
faf repoadd os_sources dnf http://linuxsoft.cern.ch/cern/centos/7/os/Sources/ --nogpgcheck
faf repoassign os_sources "CentOS 7" x86_64
faf repoadd os_vault_source dnf http://vault.centos.org/7.7.1908/os/Source/ --nogpgcheck
faf repoassign os_vault_source "CentOS 7" x86_64

faf pull-components


faf reposync --name-prefix glibc -d -v 2> /tmp/glibc.log
faf reposync --name-prefix coreutils -d -v 2> /tmp/coreutils.log
```

```
sleep 10m &
kill -SIGSEGV <ID>
```

```
faf save-reports
faf create-problems
faf retrace -v -d
```
